

# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# February 2023


# Set seed
set.seed(10)
# Length of the series
n = 1000
# Path to save the series
chemin = "C:/Users/mario/Desktop/M2 Health Data Science 2022 2023/Séries chronologiques/Assi NGuessan/Documents"


# Simulation of auto regressive process
# Simulation AR(1) with a=0.8
x = arima.sim(list(ar=c(0.8)), n)

# Visual representation
plot(x, main="AR(1) with a = c(0.8)")

# Auto correlation
acx = acf(x, lag.max=40, main="ACF of AR(1) with a = c(0.8)")

# Partial auto correlation
pacx = pacf(x, lag.max=40, main="PACF of AR(1) with a = c(0.8)")

# Save series
write.table(x, file=paste0(chemin,"/ar1.txt"), row.names = FALSE, col.names = FALSE)





### Simulation AR(1) with a = -0.8
x = arima.sim(list(ar=c(-0.8)), n)

# Visual representation
plot(x, main="AR(1) with a = c(-0.8)")

# Auto correlation
acx = acf(x, lag.max=40, main="ACF of AR(1) with a = c(-0.8)")

# Partial auto correlation
pacx = pacf(x, lag.max=40, main="PACF of AR(1) with a = c(-0.8)")

# Save series
write.table(x, file=paste0(chemin,"/ar1b.txt"), row.names = FALSE, col.names = FALSE)






### Simulation AR(1) no stationary using recurrence formula
eps = rnorm(n) # gaussian white noise
x = rep(0,length(n))
a = -1.2 #coefficient
x0 = 1 # initial value
x[1] = a*x0+eps[1] # first value of the serie

# Recurrence formula
for(i in 2:length(eps))
{
  x[i] = eps[i] + a * x[i-1]
}

# Visual representation
plot(x,type="l", main="AR(1) with a = c(-1.2)")

# Auto correlation
acx = acf(x, lag.max=40, main="ACF of AR(1) with a = c(-1.2)")

# Partial auto correlation
pacx = pacf(x, lag.max=40, main="PACF of AR(1) with a = c(-1.2)")

# Save series
write.table(x, file=paste0(chemin,"/ar1c.txt"), row.names = FALSE, col.names = FALSE)



### Simulation AR(2) with a = c(0,0.9)
x = arima.sim(list(ar=c(0,0.9)), n)

# Visual representation
plot(x, main="AR(2) with a = c(0,0.9)")

# Auto correlation
acx = acf(x, lag.max=40, main="ACF of AR(2) with a = c(0,0.9)")

# Partial Auto correlation
pacx = pacf(x, lag.max=40, main="PACF of AR(2) with a = c(0,0.9)")

# Save series
write.table(x, file=paste0(chemin,"/ar2.txt"), row.names = FALSE, col.names = FALSE)




### Simulation AR(2) with a = c(-0.5,-0.9)
x = arima.sim(list(ar=c(-0.5,-0.9)), n)

# Visual representation
plot(x, main="AR(2) with a = c(-0.5,-0.9)")

# Auto correlation
acx = acf(x, lag.max=40, main="ACF of AR(2) with a = c(-0.5,-0.9)")

# Partial Autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of AR(2) with a = c(-0.5,-0.9)")

# Save series
write.table(x, file=paste0(chemin,"/ar2b.txt"), row.names = FALSE, col.names = FALSE)




######## Simulation of mobile mean process
### Simulation MA(1) with b = c(0.8)
x = arima.sim(list(ma=c(0.8)), n)

# Visual representation
plot(x, main="MA(1) with b = c(0.8)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of MA(1) with b = c(0.8)")

# Partial Auto correlation
pacx = pacf(x, lag.max=40, main="PACF of MA(1) with b = c(0.8)")

# Save series
write.table(x, file=paste0(chemin,"/ma1.txt"), row.names = FALSE, col.names = FALSE)





### Simulation MA(1) with b = c(-0.8)
x = arima.sim(list(ma=c(-0.8)), n)

# Visual representation
plot(x, main="MA(1) with b = c(-0.8)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of MA(1) with b = c(-0.8)")

# Partial autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of MA(1) with b = c(-0.8)")

# Save serie
write.table(x, file=paste0(chemin,"/ma1b.txt"), row.names = FALSE, col.names = FALSE)




### Simulation MA(2) with b = c(0,0.9)
x = arima.sim(list(ma=c(0,0.9)), n)

# Visual representation
plot(x, main="MA(2) with b = c(0,0.9)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of MA(2) with b = c(0,0.9)")

# Partial autocorrelation 
pacx = pacf(x, lag.max=40, main="PACF of MA(2) with b = c(0,0.9)")

# Save serie
write.table(x, file=paste0(chemin,"/ma2.txt"), row.names = FALSE, col.names = FALSE)



### Simulation MA(2) with b = c(-0.5,-0.9)
x = arima.sim(list(ma=c(-0.5,-0.9)), n)

# Visual representation
plot(x, main="MA(2) with b = c(-0.5,-0.9)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of MA(2) with b = c(-0.5,-0.9)")

# Partial autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of MA(2) with b = c(-0.5,-0.9)")

# Save serie
write.table(x, file=paste0(chemin,"/ma2b.txt"), row.names = FALSE, col.names = FALSE)



################" Simulation of mixed process ARMA
### Simulation ARMA(1,1) with a=c(0.8) and b = c(0.8)
x = arima.sim(list(ar=c(0.8),ma=c(0.8)), n)

# Visual representation
plot(x, main="ARMA(1,1) with a=c(0.8) and b = c(0.8)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of ARMA(1,1) with a=c(0.8) and b = c(0.8)")

# Partial Autocorrelation 
pacx = pacf(x, lag.max=40, main="PACF of ARMA(1,1) with a=c(0.8) and b = c(0.8)")

# Save serie
write.table(x, file=paste0(chemin,"/arma11.txt"), row.names = FALSE, col.names = FALSE)




### Simulation ARMA(2,1) with a=c(0,0.8) and b = c(0.8)
x = arima.sim(list(ar=c(0,0.8),ma=c(0.8)), n)

# Visual representation
plot(x, main="ARMA(2,1) with a=c(0,0.8) and b = c(0.8)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of ARMA(2,1) with a=c(0,0.8) and b = c(0.8)")

# Partial Autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of ARMA(2,1) with a=c(0,0.8) and b = c(0.8)")

# Save serie
write.table(x, file=paste0(chemin,"/arma21.txt"), row.names = FALSE, col.names = FALSE)






### Simulation ARMA(1,2) with a=c(0.8) and b = c(0,0.8)
x = arima.sim(list(ar=c(0.8),ma=c(0,0.8)), n)

# Visual representation
plot(x, main="ARMA(1,2) with a=c(0.8) and b = c(0,0.8)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of ARMA(1,2) with a=c(0.8) and b = c(0,0.8)")

# Partial Autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of ARMA(1,2) with a=c(0.8) and b = c(0,0.8)")

# Save serie
write.table(x, file=paste0(chemin,"/arma12.txt"), row.names = FALSE, col.names = FALSE)





### Simulation ARMA(2,2) with a=c(-0.5,-0.8) and b = c(0.5,0.8)
x = arima.sim(list(ar=c(-0.5,-0.8),ma=c(0.5,0.8)), n)

# Visual representation
plot(x, main="ARMA(2,2) with a=c(-0.5,-0.8) and b = c(0.5,0.8)")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of ARMA(2,2) with a=c(-0.5,-0.8) and b = c(0.5,0.8)")

# Partial autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of ARMA(2,2) with a=c(-0.5,-0.8) and b = c(0.5,0.8)")

# Save serie
write.table(x, file=paste0(chemin,"/arma22.txt"), row.names = FALSE, col.names = FALSE)






### Simulation ARMA(1,12) with a=c(-0.611) and b[12]=0.726
x = arima.sim(list(ar=c(-0.611),ma=c(rep(0,11),0.726)), n)

# Visual representation
plot(x, main="ARMA(1,12) with a=c(-0.611) and b[12]=0.726")

# Autocorrelation
acx = acf(x, lag.max=40, main="ACF of ARMA(1,12) with a=c(-0.611) and b[12]=0.726")

# Partial autocorrelation
pacx = pacf(x, lag.max=40, main="PACF of ARMA(1,12) with a=c(-0.611) and b[12]=0.726")

# Save series
write.table(x, file=paste0(chemin,"/arma112.txt"), row.names = FALSE, col.names = FALSE)

