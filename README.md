# Time_series_Analysis


In this R file you can have a look at multiple simulations (auto correlation, partial auto correlation) with different values and visual representations. You don't need any data to run the code but you can change the path to save the different series !


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

February 2023
